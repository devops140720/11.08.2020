
# default parameters must be at the end
def print_sum(x = 2, y = 1, z = 0):
    print(x + y)

def get_add(x = 0, y = 0):
    return x + y

def get_circle_area(r):
    #print(3.14* r ** 2)
    return 3.14 * r ** 2

def get_hekef(r):
    return 3.14 * 2 * r

def index_of_item(l1, item):
    index = 0
    for n in l1:
        if n == item:
            return index
        index = index + 1
    # if you are here, the item is not inside the list
    return None

def getInRange(min, max):
    "function getInRange"
    "input number from user in a loop until the input number was in the given range"
    "min- the minimum value allowed for the input"
    "max- the maximum value allowed for the input"
    "return the number input from the user (in range)"
    # loop - input number from user
    # until the number is in range of min-max
    while True: # do-while
        x = int(input("Enter number:"))
        if x >= min and x <= max:
            return x
        print(f'{x} not in range {min}-{max}')

# while / do-while

number_in_range = getInRange(1, 20)
print_sum(y=8, x=12)
print_sum(8)
print_sum(1,2)

area = get_circle_area(4.15)

int(4.3)
